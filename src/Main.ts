function foo(x: number, y: number): number {
  return bar(x - y);
}

function bar(x: number): number {
  return baz(x, x);
}

function baz(i: number, a: number): number {
  for (let j = 0; j < Math.abs(i); j++)
    if (j % 3 == 0)
      a += foo(a, i);
  return a;
}

export function main1(output: HTMLElement) {
  output.innerText = foo(1, 5).toString();
}